# BioStudies

This R packages provides a function for downloading files associated with a
study in BioStudies.

## Installation

The packages can be installed in R using the following commands.

```
if(!require(devtools)) install.packages("devtools")
devtools::install_gitlab("wolftower/biostudies")
```
